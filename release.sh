#!/bin/bash

# This file is part of CoVeriTeam, a tool for on-demand composition of cooperative verification systems:
# https://gitlab.com/sosy-lab/software/coveriteam
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# Guide to use this script:
# 1. version as a parameter. It should be new, i.e, different than the one in the init file.
#    This is automatically updated in the init file.
# 2. Update the change log with the contents of this version.
# 3. There should be no local changes. Either commit or stash them.

set -e

if [ -z "$1" ]; then
  echo "Please specify to-be-released version as parameter."
  exit 1
fi

OLD_VERSION="$(grep __version__ coveriteam/__init__.py | sed -e 's/^.*"\(.*\)".*$/\1/')"
VERSION="$1"
if [ $(expr match "$VERSION" ".*dev") -gt 0 ]; then
  echo "Cannot release development version."
  exit 1
fi
if [ "$VERSION" = "$OLD_VERSION" ]; then
  echo "Version already exists."
  exit 1
fi
if ! grep -q "CoVeriTeam $VERSION" CHANGELOG.md; then
  echo "Cannot release version without changelog, please update CHANGELOG.md"
  exit 1
fi
if [ ! -z "$(git status -uno -s)" ]; then
  echo "Cannot release with local changes, please stash them."
  exit 1
fi

if ! which twine > /dev/null; then
  echo 'Please install twine>=1.11.0, e.g. with "pipx install twine" or "pip3 install --user twine".'
  exit 1
fi

# Prepare files with new version number
sed -e "s/^__version__ = .*/__version__ = \"$VERSION\"/" -i coveriteam/__init__.py
git commit coveriteam/__init__.py -m "Release $VERSION"


# Other preparations
DIR="$(pwd)"
DIST_DIR="$DIR/dist-$VERSION"
rm -r "$DIST_DIR" 2>/dev/null || true
mkdir "$DIST_DIR"

# Test and build under Python 3
TEMP3="$(mktemp -d)"
virtualenv -p /usr/bin/python3 "$TEMP3"
. "$TEMP3/bin/activate"
git clone "file://$DIR" "$TEMP3/coveriteam"
pushd "$TEMP3/coveriteam"
pip install -e "."
pip install 'wheel>=0.32.0' 'setuptools>=42.0.0'
# python setup.py nosetests
python setup.py sdist bdist_wheel
popd
deactivate
cp "$TEMP3/coveriteam/dist/"* "$DIST_DIR"
rm -rf "$TEMP3"


for f in "$DIST_DIR/"*; do
  gpg --detach-sign -a "$f"
done
git tag -s "$VERSION" -m "Release $VERSION"


# Upload and finish
read -p "Everything finished, do you want to release version '$VERSION' publically? (y/n) " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]; then
  exit 0
fi

git push --tags
twine upload "$DIST_DIR/CoVeriTeam"*

read -p "Please enter next version number:  " -r
sed -e "s/^__version__ = .*/__version__ = \"$REPLY\"/" -i coveriteam/__init__.py
git commit coveriteam/__init__.py -m"Prepare version number for next development cycle."


echo
